// This is the canonical layout file for the Quantum project. If you want to add another keyboard,
// this is the style you want to emulate.

#include "kc60.h"
#ifdef BACKLIGHT_ENABLE
  #include "backlight.h"
#endif
#ifdef RGBLIGHT_ENABLE
  #include "rgblight.h"
#endif

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [0] = KEYMAP( /* Basic QWERTY */
      F(2),     KC_1,     KC_2,     KC_3,    KC_4,     KC_5,     KC_6,     KC_7,    KC_8,  KC_9,     KC_0,     KC_MINS,  KC_EQL,   KC_BSPC,  \
      KC_TAB,   KC_Q,     KC_W,     KC_E,    KC_R,     KC_T,     KC_Y,     KC_U,    KC_I,  KC_O,     KC_P,     KC_LBRC,  KC_RBRC,  KC_BSLS,  \
      KC_LCTL,  KC_A,     KC_S,     KC_D,    KC_F,     KC_G,     KC_H,     KC_J,    KC_K,  KC_L,     KC_SCLN,  KC_QUOT,  KC_NO,    KC_ENT,   \
      F(11),    KC_NO,    KC_Z,     KC_X,    KC_C,     KC_V,     KC_B,     KC_N,    KC_M,  KC_COMM,  KC_DOT,   KC_SLSH,  KC_NO,    KC_RSFT,  \
      KC_LCTL,  KC_LGUI,  KC_LALT,                     F(1),                                         KC_RALT,  KC_RGUI,  F(0),     KC_RCTL),
  [1] = KEYMAP( /* First Fn Layer */
      KC_GRV,   KC_F1,    KC_F2,    KC_F3,   KC_F4,    KC_F5,    KC_F6,    KC_F7,   KC_F8,  KC_F9,      KC_F10,   KC_F11,   KC_F12,   KC_DEL,   \
      KC_TAB,   KC_Q,     KC_UP,    KC_E,    KC_R,     KC_T,     KC_Y,     KC_U,    KC_INS, KC_O,       KC_PSCREEN,  KC_SLCK,  KC_PAUS,  KC_BSLS,  \
      KC_CAPS,  KC_LEFT,  KC_DOWN,  KC_RGHT, KC_F,     KC_G,     KC_H,     KC_J,    KC_K,   LGUI(KC_L), KC_HOME,  KC_PGUP,  KC_NO,    KC_ENT,   \
      KC_LSFT,  KC_NO,    KC_Z,     KC_X,    BL_DEC,   BL_TOGG,  BL_INC,   KC_N,    KC_M,   KC_COMM,    KC_END,   KC_PGDN,  KC_NO,    KC_RSFT,  \
      KC_LCTL,  KC_LGUI,  KC_TRNS,                     KC_TRNS,                                         KC_RALT,  KC_RGUI,  KC_TRNS,  KC_RCTL),
  [2] = KEYMAP( /* Mousekeys Layer */
      KC_ESC,   KC_1,     KC_BTN3,  KC_TRNS, KC_4,     KC_5,     KC_6,     KC_P7,   KC_P8, KC_P9,    KC_0,     KC_MINS,  KC_EQL,   KC_BSPC,  \
      KC_TAB,   KC_BTN1,  KC_MS_U,  KC_BTN2, KC_R,     KC_T,     KC_Y,     KC_P4,   KC_P5, KC_P6,    KC_NLCK,  KC_LBRC,  KC_RBRC,  KC_BSLS,  \
      KC_WH_U,  KC_MS_L,  KC_MS_D,  KC_MS_R, KC_F,     KC_G,     KC_H,     KC_P1,   KC_P2, KC_P3,    KC_PPLS,  KC_PAST,  KC_NO,    KC_ENT,   \
      KC_WH_D,  KC_NO,    KC_Z,     KC_X,    KC_C,     KC_V,     KC_B,     KC_N,    KC_P0, KC_COMM,  KC_PDOT,  KC_PSLS,  KC_NO,    KC_RSFT,  \
      KC_LCTL,  KC_LGUI,  KC_LALT,                     KC_TRNS,                                      KC_RALT,  KC_RGUI,  KC_TRNS,  KC_RCTL),
  [3] = KEYMAP( /* RGBLIGHT */
      KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,  KC_TRNS,  KC_TRNS, \
      KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,  KC_TRNS,  KC_TRNS, \
      KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,  KC_NO,    KC_TRNS, \
      KC_TRNS,  KC_NO,    F(3),     F(4),    F(5),     F(6),     F(7),     F(8),    F(9),    F(10),   KC_TRNS, KC_TRNS,  KC_NO,    KC_TRNS, \
      KC_TRNS,  KC_TRNS,  KC_TRNS,                     KC_TRNS,                                       KC_TRNS, KC_TRNS,  KC_TRNS,  KC_TRNS),
  [4] = KEYMAP( /* tilde - grave */
      KC_GRV,  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,  KC_TRNS,  KC_TRNS, \
      KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,  KC_TRNS,  KC_TRNS, \
      KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,  KC_NO,    KC_TRNS, \
      KC_TRNS,  KC_NO,    KC_TRNS,  KC_TRNS, KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,  KC_NO,    KC_TRNS, \
      KC_TRNS,  KC_TRNS,  KC_TRNS,                     KC_TRNS,                                       KC_TRNS, KC_TRNS,  KC_TRNS,  KC_TRNS),
      
};

enum function_id {
    RGBLED_TOGGLE,
    RGBLED_STEP_MODE,
    RGBLED_INCREASE_HUE,
    RGBLED_DECREASE_HUE,
    RGBLED_INCREASE_SAT,
    RGBLED_DECREASE_SAT,
    RGBLED_INCREASE_VAL,
    RGBLED_DECREASE_VAL,
};

const uint16_t PROGMEM fn_actions[] = {
  [0]  = ACTION_LAYER_TOGGLE(2),
  [1]  = ACTION_LAYER_TAP_KEY(1, KC_SPC),
  [2]  = ACTION_LAYER_TAP_KEY(3, KC_ESC),
  [3]  = ACTION_FUNCTION(RGBLED_TOGGLE),
  [4]  = ACTION_FUNCTION(RGBLED_STEP_MODE),
  [5]  = ACTION_FUNCTION(RGBLED_INCREASE_HUE),
  [6]  = ACTION_FUNCTION(RGBLED_DECREASE_HUE),
  [7]  = ACTION_FUNCTION(RGBLED_INCREASE_SAT),
  [8]  = ACTION_FUNCTION(RGBLED_DECREASE_SAT),
  [9]  = ACTION_FUNCTION(RGBLED_INCREASE_VAL),
  [10] = ACTION_FUNCTION(RGBLED_DECREASE_VAL),
  [11] = ACTION_LAYER_MODS(4, MOD_LSFT),
};


void action_function(keyrecord_t *record, uint8_t id, uint8_t opt) {
  switch (id) {
    case RGBLED_TOGGLE:
      //led operations
      if (record->event.pressed) {
        rgblight_toggle();
      }
      break;
    case RGBLED_INCREASE_HUE:
      if (record->event.pressed) {
        rgblight_increase_hue();
      }
      break;
    case RGBLED_DECREASE_HUE:
      if (record->event.pressed) {
        rgblight_decrease_hue();
      }
      break;
    case RGBLED_INCREASE_SAT:
      if (record->event.pressed) {
        rgblight_increase_sat();
      }
      break;
    case RGBLED_DECREASE_SAT:
      if (record->event.pressed) {
        rgblight_decrease_sat();
      }
      break;
      case RGBLED_INCREASE_VAL:
        if (record->event.pressed) {
          rgblight_increase_val();
        }
        break;
      case RGBLED_DECREASE_VAL:
        if (record->event.pressed) {
          rgblight_decrease_val();
        }
        break;
      case RGBLED_STEP_MODE:
        if (record->event.pressed) {
          rgblight_step();
        }
        break;
  }
}
